<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;

class LeaderboardController extends Controller
{
    public function index() {
        $users = DB::table('match_user')
            ->join('users', 'match_user.user_id', '=', 'users.id')
            ->select(DB::raw('users.id as id, users.name as name, AVG(score) as avg, MAX(score) as max, count(user_id) as matches'))
            ->groupBy('id', 'users.name')
            ->havingRaw('count(user_id) >= 10')
            ->orderBy('avg', 'desc')
            ->limit(10)
            ->get();

        return view('leaderboard.index', compact('users'));

        /*
        $users = DB::select(DB::raw('SELECT users.id as user_id,
          users.name as "name",
          AVG(match_user.score) as "AVG",
          MAX(match_user.score) as "MAX",
          COUNT(user_id) as "matches" FROM match_user
          JOIN users ON match_user.user_id=users.id
          GROUP BY user_id
          ORDER BY MAX DESC'));

        return $users;
        */

        /*
        To get wins use.
        select user_id,
        COUNT(match_id) as wins
        from match_user
        where (match_id, score) in (
            select match_id, MAX(score)
            from match_user
            group by match_id)
        group by user_id
        */
    }
}
