<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class SessionController extends Controller
{
    /**
    * Logsout the current user.
    *
    * @return redirect
    */
    public function destroy() {
        Auth::logout();
        return redirect('/');
    }
}
