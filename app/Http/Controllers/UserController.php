<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Carbon\Carbon;

class UserController extends Controller
{
    /*
    * Index of all users.
    *
    * @return view with all users.
    */
    public function index() {
        $users = User::paginate(10);
        return view('users.index', compact('users'));
    }

    /*
    * Profile page for specified user.
    *
    * @param  User
    * @return page with users.
    */
    public function show(User $user) {
        if (!$user->matches->isEmpty()) {
            $data = $user->getData();
        }
        // return $data;
        return view('users.show', compact('user', 'data'));
    }

    /*
    * Edit page for specified user.
    *
    * @param  User
    * @return edit view for user.
    */
    public function edit(User $user) {
        return view('users.edit', compact('user'));
    }

    /*
    * Update a user's details.
    *
    * @param  Request
    * @param  User
    * @return redirect to user page.
    */
    public function update(Request $request, User $user) {
        $user = User::find($user);
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'phone' => 'required|max:30',
        ]);
        $user->name = $request['name'];
        $user->email = $request['email'];
        $user->phone = $request['phone'];
        $user->save();
        session()->flash('message', 'User was successfully updated.');
        return redirect()->back();
    }

}
