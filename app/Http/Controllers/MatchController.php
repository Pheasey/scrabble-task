<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Match;

class MatchController extends Controller
{
    /*
    * Index of all matches.
    *
    * @return view with all matches.
    */
    public function index() {
        $matches = Match::paginate(10);
        return view('matches.index', compact('matches'));
    }

    /*
    * Info page for specified matches.
    *
    * @param  Match
    * @return page with match details.
    */
    public function show(Match $match) {
        $users = $match->users;
        return view('matches.show', compact('match', 'users'));
    }

    /*
    * Creation page for a new match.
    *
    * @return create view for a match.
    */
    public function create() {
        return view('matches.create');
    }

    /*
    * Edit page for specified match.
    *
    * @param  match
    * @return edit view for a match.
    */
    public function edit(Match $match) {
        return view('matches.show', compact('match'));
    }


    /*
    * Creation of a new match.
    *
    * @param  request from form
    * @return created match page
    */
    public function make(Request $request) {
        $this->validate($request, [
            'played_at' => 'required|date',
            'duration' => 'required|numeric'
        ]);
        $match = new Match();
        $match->played_at = $request['played_at'];
        $match->duration = $request['duration'];
        $match->save();
        return redirect("/matches/{$match->id}");
    }

    /*
    * User addition for matchs.
    *
    * @param  request from form
    * @return match page
    */
    public function addUser(Request $request) {
        $this->validate($request, [
            'match_id' => 'required|numeric',
            'user_id' => 'required',
            'score' => 'required|numeric',
        ]);
        $match = Match::find($request['match_id']);
        $winner = false;
        if ($request['winner'] != null) {
            $winner = true;
        }
        $match->users()->attach($request['user_id'], ['score' => $request['score'], 'winner' => $winner]);
        return redirect("/matches/{$match->id}");
    }

    /*
    * Creation of edit/add user page.
    *
    * @param  match
    * @return edit match_user page
    */
    public function editUsers(Match $match) {
        return view('matches.editUsers', compact('match'));
    }

}
