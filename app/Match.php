<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    /**
    * Link between matches and users.
    *
    * @return users and scores from pivot table
    */
    public function users() {
        return $this->belongsToMany(User::class, 'match_user')->withPivot('score')->orderBy('pivot_score', 'desc');
    }
}
