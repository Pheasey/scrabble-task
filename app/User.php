<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
    * Link between users and matches.
    *
    * @return matches and scores from pivot table
    */
    public function matches() {
        return $this->belongsToMany(Match::class, 'match_user')->withPivot('score', 'winner');
    }

    /**
    * Gets user data for profile pages.
    *
    * @return data
    */
    public function getData() {
      return array_merge($this->getWLData(), $this->getBestMatchData());
    }

    /**
    * Gets the stats about the users matches.
    *
    * @return data
    */
    public function getWLData() {
        $wlData = [
            'wins' => $this->matches()->wherePivot('winner', '=', 1)->count(),
            'losses' => $this->matches()->wherePivot('winner', '!=', 1)->count(),
            'matches' => $this->matches()->count(),
            'avg' => $this->matches()->avg('score'),
        ];
        return $wlData;
    }

    /**
    * Gets the data from the users best match.
    *
    * @return data
    */
    public function getBestMatchData() {
      $match = $this->matches()->orderBy('score', 'DESC')->first();
      $matchData = [
          'match' => $match->id,
          'score' => $match->pivot->score,
          'date' => $match->played_at,
          'opponent' => $match->users->where('id', '!=', $this->id)->pluck('name')->pop(),
      ];
      return $matchData;
    }

}
