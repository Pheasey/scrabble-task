@extends('layouts.master')

@section('title', 'Register')

@section('content')

  <div class="panel-block">
    <form role="form" method="POST" action="{{ route('register') }}">
        {{ csrf_field() }}

        <label for="name" class="col-md-4 control-label">Name</label>

        <input class="input" id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif

        <label for="email" class="col-md-4 control-label">E-Mail Address</label>
        <input class="input" id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif

        <label for="phone" class="col-md-4 control-label">Phone Number</label>
        <input class="input" id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" required>

        @if ($errors->has('phone'))
            <span class="help-block">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif

        <label for="password" class="col-md-4 control-label">Password</label>
        <input class="input" id="password" type="password" class="form-control" name="password" required>

        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif

        <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
        <input class="input" id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
        <hr>
        <button type="submit" class="button is-primary">Register</button>
    </form>

    <br>

    @include('layouts.errors')

  </div>

@endsection
