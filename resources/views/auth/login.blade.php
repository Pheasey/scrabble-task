@extends('layouts.master')

@section('title', 'Login')

@section('content')

  <div class="panel-block">
    <form role="form" method="POST" action="{{ route('login') }}">
      {{ csrf_field() }}

      <label for="email" class="col-md-4 control-label">E-Mail Address</label>
      <input class="input" id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

      @if ($errors->has('email'))
          <span class="help-block">
              <strong>{{ $errors->first('email') }}</strong>
          </span>
      @endif

      <label for="password" class="col-md-4 control-label">Password</label>
      <input class="input" id="password" type="password" class="form-control" name="password" required>

      @if ($errors->has('password'))
          <span class="help-block">
              <strong>{{ $errors->first('password') }}</strong>
          </span>
      @endif

      <div class="column">
        <label>
          <input class="checkbox" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
        </label>
        <button class="button is-primary" type="submit">Login</button>
        <a class="button" href="{{ route('password.request') }}">Forgot Your Password?</a>
      </div>


    </form>
  </div>

@endsection
