@extends('layouts.master')

@section('title')
  Matches <small>({{ $matches->firstItem() }} to {{ $matches->lastItem() }} / {{ $matches->total() }})</small>
@endsection


@section('content')

  <div class="panel-block">
    <a class="button is-primary" href="matches/create">New Match</a>
  </div>

  @foreach ($matches as $match)

    @include('matches.match')

  @endforeach

  <div class="panel-block">

    {{ $matches->links() }}

  </div>

@endsection
