<div class="panel-block">
  <article class="media">
    <figure class="media-left">
    </figure>
    <div class="media-content">
      <div class="content">
        <p>
          <strong><a href="/matches/{{ $match->id }}">Match Number {{ $match->id }}</a></strong> <small>id#{{ $match->id }}</small>
          <br>
          Date: {{ $match->played_at }}
          <br>
          Duration: {{ gmdate("H:i:s", $match->duration) }}
          <br>
        </p>
      </div>
    </div>
  </article>
</div>
