@extends('layouts.master')

@section('title')
  Match Info: {{ $match->id }}
@endsection

@section('content')

  @include('matches.match')

  @include('matches.details')

  <div class="panel-block">
    <a class="button is-primary" href="/matches/{{ $match->id }}/users">Add Players</a>
  </div>

@endsection
