@extends('layouts.master')

@section('title', 'New Match')

@section('content')

  <div class="panel-block">

    <form method="POST" action="/matches">

      {{ csrf_field() }}

      <label for="played">Played at</label>
      <p class="control">
        <input class="input" type="date" name="played_at">
      </p>

      <label for="played">Duration (seconds)</label>
      <p class="control">
        <input class="input" type="number" name="duration">
      </p>

      <button type="submit" class="button is-primary">Create</button>

    </form>

    @include('layouts.errors')

  </div>


@endsection
