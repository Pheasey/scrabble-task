<div class="panel-block">
  <div class="media-content">
    <div class="content">

      <h4 class="title">Results</h4>

      @include('matches.table')

    </div>
  </div>
</div>
