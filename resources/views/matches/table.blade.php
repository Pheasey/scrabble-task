<table class="table">
  <thead>
    <tr>
      <th>Player</th>
      <th>Points</th>
    </tr>
  </thead>
  <tbody>

    @foreach ($match->users as $user)

      <tr>
        <td><a href="/users/{{ $user->id }}">{{ $user->name }}</a> <small>id#{{ $user->id }}</small></td>
        <td>{{ $user->pivot->score }}</td>
      </tr>

    @endforeach

  </tbody>
</table>
