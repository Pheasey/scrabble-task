@extends('layouts.master')

@section('title')
  Edit Players in Match #{{ $match->id }}
@endsection

@section('content')

  @include('matches.match')

  @include('matches.details')

  <div class="panel-block">

    <form method="POST" action="/matches/{{ $match->id }}/add">

      {{ csrf_field() }}

      <input type="hidden" name="match_id" value="{{ $match->id }}">

      <p class="control">
        <label for="user_id">Player ID:</label>
        <input class="input" type="number" name="user_id" id="user_id" max="{{ App\User::count() }}" required>
      </p>

      <p class="control">
        <label for="score">Points:</label>
        <input class="input" type="number" name="score" id="score" required>
      </p>

      <p class="control">
        <label class="checkbox">Winner:</label>
          <input name="winner" id="winner" type="checkbox">
      </p>

      <button type="submit" class="button is-primary">Add Player</button>

      @if($flash = session('message'))

        @include('layouts.success')

      @endif

      @include('layouts.errors')

    </form>

  </div>

@endsection
