@extends('layouts.master')

@section('title', 'Home')

@section('content')

  <div class="panel-block">
    <aside class="menu">
      <ul class="menu-list">
        <li><a href="{{ url('/users') }}"><i class="fa fa-users fa-6" aria-hidden="true"></i> Users</a></li>
        <li><a href="{{ url('/matches') }}"><i class="fa fa-trophy fa-6" aria-hidden="true"></i> Matches</a></li>
        <li><a href="{{ url('/leaderboard') }}"><i class="fa fa-list-ol fa-6" aria-hidden="true"></i> Leaderboard</a></li>
      </ul>
    </aside>
  </div>

@endsection
