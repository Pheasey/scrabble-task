<div class="nav">
  <div class="nav-left">
    <h1 class="nav-item title is-3"><a href="/">Scrabble Club</a></h1>
  </div>
  <div class="nav-right">
    @if (!Auth::check())
        <a class="nav-item" href="{{ url('/login') }}">Login</a>
        <a class="nav-item" href="{{ url('/register') }}">Register</a>
    @else
      <a class="nav-item" href="/users/{{ Auth::user()->id }}">My Profile</a>
      <a class="nav-item" href="{{ url('/logout') }}">Logout</a>
    @endif
    <a class="nav-item" href="{{ url('/users') }}">Users</a>
    <a class="nav-item" href="{{ url('/matches') }}">Matches</a>
    <a class="nav-item" href="{{ url('/leaderboard') }}">Leaderboard</a>
  </div>
</div>
