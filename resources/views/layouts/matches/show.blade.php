@extends('layouts.master')

@section('content')

  <div class="columns">
    <div class="column">
      <div class="panel">
          <p class="panel-heading">Match Info: {{ $match->id }}</p>

          @include('matches.match')

      </div>
    </div>
  </div>

@endsection
