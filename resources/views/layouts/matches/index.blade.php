@extends('layouts.master')

@section('content')

  <div class="columns">
    <div class="column">
      <div class="panel">
          <p class="panel-heading">Matches</p>

          @foreach ($matches as $match)

            @include('matches.match')

          @endforeach

      </div>
    </div>
  </div>

@endsection
