<footer class="footer">
  <div class="container">
    <div class="content has-text-centered">
      <p>
        <strong>Scrabble Club</strong> by Andrew Pheasey
        <p>Created using <a href="https://laravel.com/">Laravel</a> and <a href="http://bulma.io">Bulma</a></p>
      </p>
      <p>
        <a class="icon" href="https:///gitlab.com/Pheasey/scrabble-task">
          <i class="fa fa-gitlab"></i>
        </a>
      </p>
    </div>
  </div>
</footer>
