<div class="panel-block">
  <article class="media">
    <figure class="media-left">
      <p class="image is-64x64">
        <img src="https://robohash.org/{{ $user->id }}.png">
      </p>
    </figure>
    <div class="media-content">
      <div class="content">
        <p>
          <strong><a href="/users/{{ $user->id }}">{{ $user-> name }}</a></strong> <small>id#{{ $user->id }}</small>
          <br>
          Email: {{ $user->email }}
          <br>
          Phone Number: {{ $user->phone }}
          <br>
          Joined: {{ $user->created_at->diffForHumans() }}
        </p>
      </div>
    </div>
  </article>
</div>
