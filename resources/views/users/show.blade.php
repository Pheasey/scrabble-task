@extends('layouts.master')

@section('title')
  User Info: {{ $user->name }}
@endsection

@section('content')

  @include('users.user')

  @include('users.details')

  <div class="panel-block">
      <a class="button" href="/users/{{ $user->id }}/edit">Edit User</a>
  </div>

@endsection
