@extends('layouts.master')

@section('title')
  Editing User: {{ $user->name }}
@endsection

@section('content')

  @include('users.user')

  <div class="panel-block">

    <form method="POST" action="/users/{{ $user->id }}">

      {{ csrf_field() }}
      {{ method_field('PUT') }}

      <p class="control">
        <label for="username">Name:</label>
        <input class="input" type="text" name="name" id="name" value="{{ $user->name }}">
      </p>

      <p class="control">
        <label for="username">Email:</label>
        <input class="input" type="text" name="email" id="email" value="{{ $user->email }}">
      </p>

      <p class="control">
        <label for="phone">Phone Number:</label>
        <input class="input" type="text" name="phone" id="phone" value="{{ $user->phone }}">
      </p>

      <button type="submit" class="button is-primary">Update</button>

      @if($flash = session('message'))

        @include('layouts.success')

      @endif

      @include('layouts.errors')

    </form>

  </div>

@endsection
