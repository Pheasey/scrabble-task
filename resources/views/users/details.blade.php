<div class="panel-block">
  <div class="media-content">
    <div class="content">

      <div class="columns">
        <div class="column">
          <h4>Match History</h4>
          <ul>

            @if ($user->matches->isEmpty())

              <i>No matches currently on record for {{ $user->name }}.</i>

            @endif

            @foreach ($user->matches->slice(0, 8) as $match)

              <li><a href="/matches/{{ $match->id }}">Match {{ $match->id }}</a><small>, {{ Carbon\Carbon::parse($match->played_at)->diffForHumans() }}</small></li>

            @endforeach

            @if ($user->matches->count()-8 > 0)
                <li>And {{ ($user->matches->count())-8 }} more..</li>
            @endif

          </ul>
        </div>
        <div class="column">
          <h4>Stats Overview</h4>
          <ul>

            @if ($user->matches->isEmpty())

              <i>No statistics currently avaiable for {{ $user->name }}.</i>

            @else

              <li>Wins: {{ $data['wins'] }}</li>
              <li>Losses: {{ $data['losses'] }}</li>
              <li>Matches: {{ $data['matches'] }}</li>
              <li>Average Score: {{ $data['avg'] }}</li>

              <li>Best Match:</li>
                 <ul>
                   <li>Score: {{ $data['score'] }}</li>
                   <li>Where:  <a href="/matches/{{ $data['match'] }}">Match {{ $data['match'] }}</a></li>
                   <li>Opponent: {{ $data['opponent'] }}</li>
                   <li>When: {{ $data['date'] }}</li>
                 </ul>



              {{-- <li>Wins: {{ $user->getWinTotal() }}</li>
              <li>Losses: {{ $user->getLossTotal() }}</li>
              <li>Matches: {{ $user->matches->count() }}</li>
              <li>Highest Score: {{ $user->getHighestScore() }} in <a href="/matches/">Match {{ $user->getHighestMatch()->id }}</a></li> --}}

            @endif

          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
