@extends('layouts.master')

@section('title')
   Users <small>({{ $users->firstItem() }} to {{ $users->lastItem() }} / {{ $users->total() }})</small>
@endsection

@section('content')

  @foreach ($users as $user)

    @include('users.user')

  @endforeach

  <div class="panel-block">

    {{ $users->links() }}

  </div>

@endsection
