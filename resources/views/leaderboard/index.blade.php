@extends('layouts.master')

@section('title', 'Leaderboard')

@section('content')

  <div class="panel-block">
    <table class="table">
      <thead>
        <tr>
          <th>Player</th>
          <th>Games Played</th>
          <th>Average Score</th>
          <th>Highest Score</th>
        </tr>
      </thead>
      <tbody>

        @foreach ($users as $user)

          <tr>
            <td><a href="/users/{{ $user->id }}">{{ $user->name }}</a> <small>id#{{ $user->id }}</small></td>
            <td>{{ $user->matches }}</td>
            <td>{{ number_format($user->avg, 2) }}</td>
            <td>{{ $user->max }}</td>
          </tr>

        @endforeach

      </tbody>
    </table>
  </div>

@endsection
