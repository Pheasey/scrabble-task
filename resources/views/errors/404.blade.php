@extends('layouts.master')

@section('title', 'Error 404')

@section('content')

  <div class="panel-block">

    <p>{{ $exception }}</p>

</div>

@endsection
