<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.index');
});

// Auth Route
Auth::routes();
Route::get('/logout', 'SessionController@destroy');

// User Routes
Route::get('/users', 'UserController@index');
Route::get('/users/{user}', 'UserController@show');
Route::get('/users/{user}/edit', 'UserController@edit');
Route::put('users/{user}', 'UserController@update');
Route::post('/users/{user}', 'UserController@show');

// Match Routes
Route::get('/matches', 'MatchController@index');
Route::get('/matches/create', 'MatchController@create');
Route::get('/matches/{match}', 'MatchController@show');
Route::get('/matches/{match}/edit', 'MatchController@edit');
Route::get('/matches/{match}/users', 'MatchController@editUsers');
Route::post('/matches/{match}/add', 'MatchController@addUser');
Route::post('/matches', 'MatchController@make');

// Leaderboard Routes
Route::get('/leaderboard', 'LeaderboardController@index');

/**
* -- TODO --
* x CRUD user forms.
* x Carbon date formatting.
* x 'Scrabble Club' branding in confs.
* - Leaderboard query.
* x Data factories.
* x README customisation.
* x Fix HTML repetition.
*/
