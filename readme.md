# Scrabble Club
#### [Elder Studio Developer Exercise](http://www.elder-studios.co.uk/developer-exercise/)

This repository contains my solution for the Scrabble Club task set by [Elder Studios](http://www.elder-studios.co.uk).
Made using [Laravel](https://laravel.com/) PHP Framework and [Bulma](http://bulma.io/) CSS Framework.
#### To Run
These settings for the database can be changed in the `env.` file.
To build the project and data the following commands are required.
 - php artisan migrate
 - php artisan db:seed
 - php artisan serve
