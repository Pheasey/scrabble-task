<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      $users = 20;
      $matches = 200;

      // Create Users
      factory(App\User::class, $users)->create();
      // Create Matches
      factory(App\Match::class, $matches)->create();

      for ($i = 0; $i <= $matches; $i++) {

          $user1 = rand(0,$users);
          $user2 = $this->getRandomUser(0,$users, $user1);
          $score1 = rand(200,500);
          $score2 = $score1 - rand(50,150);

          DB::table('match_user')->insert(['match_id' => $i,'user_id' => $user1, 'score'=>$score1, 'winner' => true]);
          DB::table('match_user')->insert(['match_id' => $i,'user_id' => $user2, 'score'=>$score2]);
      }
    }

    public function getRandomUser($min, $max, $value) {
      do {
        $randomValue = rand($min, $max);
      } while ($randomValue == $value);
        return $randomValue;
    }

}
